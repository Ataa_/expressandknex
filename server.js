const app = require('./app');
const http = require('http');

const server = new http.createServer(app);

server.listen(4000,()=>{
    console.log('server is listening on : '+4000);
});