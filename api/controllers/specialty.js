const knex = require('../../knex');
const uuid = require('uuid').v4;



//get all specialties, inner join to get the category name 
module.exports.getall = (req,res,next)=>{
   knex.from('specialty as S').select('S.id','name_','category')
   .innerJoin('category',{'category.id':'S.category_id'})
    .then((rows)=>{
        res.status(200).json({
            'count':rows.length,
            'specialties':rows
        });
    }).catch((error)=>{
        console.log(error);
        res.status(500).json({
            message:'an error has occured'
        });
    })
};
//get the specialty with the provided ID , inner join to get the category.
module.exports.getOne = (req,res,next)=>{
    knex.from('specialty as S')
    .where({'S.id':req.params.id})
    .select('S.id','name_','category')
    .innerJoin('category',{'category.id':'S.category_id'})
    .then((rows)=>{
         res.status(200).json({
             'count':rows.length,
             'specialties':rows
         });
     }).catch((error)=>{
         console.log(error);
         res.status(500).json({
             message:'an error has occured'
         });
     })
 };

//get all specialties where category = something
module.exports.getAllWithCategory = (req,res,next)=>{
    knex.from('specialty as S')
    .where({'category.category':req.params.id})
    .select('S.id','name_','category')
    .innerJoin('category',{'category.id':'S.category_id'})
    .then((rows)=>{
         res.status(200).json({
             'count':rows.length,
             'specialties':rows
         });
     }).catch((error)=>{
         console.log(error);
         res.status(500).json({
             message:'an error has occured'
         });
     })
 };

module.exports.createOne = async(req,res,next)=>{
    let newSpecialty = {};
    newSpecialty.name_ = req.body.specialty;
    newSpecialty.id = uuid();
    await knex.select('id').where('category',req.body.category).from('category')
    .then((categoryID)=>{
        newSpecialty.category_id = categoryID[0].id;
        knex('specialty').insert(newSpecialty)
        .then((row)=>{
            res.status(201).json({
                result : row
            })
        }).catch((error)=>{
            console.log(error);
            res.status(500).json({
                message:'an error has occured'
            });
    })
    }).catch((error)=>{
        console.log(error);
        res.status(404).json({
            message:'this category does not exist'
        });
    })
}

module.exports.updateOne = (req,res,next)=>{
    knex('specialty').where({'id':req.params.id})
    .update(req.body)
    .then((row)=>{
        res.status(204).json({
            message : 'specialty updated successfully.'
        });
    }).catch((error)=>{
        console.log(error);
        res.status(500).json({
            message:'an error has occured'
        });
    });
}

module.exports.deleteOne = (req,res,next)=>{
    knex('specialty').where({'id':req.params.id}).del()
    .then((row)=>{
        res.status(204).json({
            message : 'specialty deleted successfully.'
        });
    }).catch((error)=>{
        console.log(error);
        res.status(500).json({
            message:'an error has occured'
        });
    });
}