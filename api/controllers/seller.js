const knex = require('../../knex');
const uuid = require('uuid').v4;


module.exports.createOne = async(req,res,next)=>{
    let newSeller = req.body;
    let type = req.body.type_;
    newSeller.id = uuid();
    type = await knex.select('id').where({'name_':type}).from('type_');
    newSeller.type_id = type[0].id;
    await knex.select('id').where('category',req.body.category).from('category')
    .then((categoryID)=>{
        newSeller.category_id = categoryID[0].id;
        delete newSeller['category'];
        delete newSeller['type_'];
        knex('seller').insert(newSeller)
        .then((row)=>{
            res.status(201).json({
                result : row
            })
        }).catch((error)=>{
            console.log(error);
            if(error.number==2627)
            res.status(409).json({
                message:'this email already exists'
            });
            else
            res.status(500).json({
                message:'an error occured'
            });
        });
    }).catch((error)=>{
        
        res.status(409).json({
            message:'this category does not exist'
        });
    })
}

module.exports.getAll = async(req,res,next)=>{
    knex.select('S.id','S.name_','S.email','C.category','T.name_ as type','S.commercial_number','S.state_')
    .from('seller as S')
    .innerJoin('category as C',{'C.id':'S.category_id'})
    .innerJoin('type_ as T',{'T.id':'S.type_id'})
    .then((rows)=>{
        res.status(200).json({
            count : rows.length,
            sellers : rows
        });
    }).catch((error)=>{
        res.status(500).json({
            error : error
        })
    });
}

module.exports.getAllwithCategory = async(req,res,next)=>{
    console.log(req.params.category);
    knex.select('S.id','S.name_','S.email','C.category','T.name_ as type','S.commercial_number','S.state_')
    .from('seller as S')
    .innerJoin('category as C',{'C.id':'S.category_id'})
    .innerJoin('type_ as T',{'T.id':'S.type_id'})
    .where({'C.category':req.params.category})
    .then((rows)=>{
        res.status(200).json({
            count : rows.length,
            sellers : rows
        });
    }).catch((error)=>{
        console.log(error)
        res.status(500).json({
            error : error
        })
    });
}

module.exports.getAllwithType = async(req,res,next)=>{
    try{
        let rows = await knex.select('S.id','S.name_','S.email','C.category','T.name_ as type','S.commercial_number','S.state_')
        .from('seller as S')
        .innerJoin('category as C',{'C.id':'S.category_id'})
        .innerJoin('type_ as T',{'T.id':'S.type_id'})
        .where({'T.name_':req.params.type})
        res.status(200).json({
            count : rows.length,
            sellers : rows
        });
    }catch(error){
        res.status(500).json({
            error : error
        })
    }
}

module.exports.getOne = async(req,res,next)=>{
    try{
    let rows = await knex.from('seller as S')
    .where({'S.id':req.params.id})
    .select('S.id','S.name_','S.email','T.name_ as type','S.commercial_number','S.state_','C.category as category')
    .innerJoin('type_ as T',{'S.type_id':'T.id'})
    .innerJoin('category as C',{'C.id':'S.category_id'})
    res.status(200).json({
        count : rows.length,
        sellers : rows
    });
    }catch(error){
        res.status(500).json({
            error : error
        })
    }
}
 
module.exports.updateOne = async(req,res,next)=>{
    knex('seller').where({'id':req.params.id})
    .update(req.body)
    .then((row)=>{
        res.status(204).json({
            message : 'seller updated successfully.'
        });
    }).catch((error)=>{
        console.log(error);
        res.status(500).json({
            message:'an error has occured'
        });
    });
}

module.exports.deleteOne = (req,res,next)=>{
    knex('seller').where({'id':req.params.id}).del()
    .then((row)=>{
        res.status(204).json({
            message : 'seller deleted successfully.'
        });
    }).catch((error)=>{
        console.log(error);
        res.status(500).json({
            message:'an error has occured'
        });
    });
}