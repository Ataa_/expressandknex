const knex = require('../../knex');
const uuid = require('uuid').v4;



module.exports.getAll = (req,res,next)=>{
    knex
    .select('t.id','t.name_ as type','c.category as category')
    .from('type_ as t')
    .innerJoin('category as c',{'t.category_id':'c.id'})
    .then((rows)=>{
        res.status(201).json({
            count : rows.length,
            types : rows
        })
    }).catch((error)=>{
        console.log(error);
        res.status(500).json({
            message:'this category does not exists'
        });
    })
}
module.exports.getAllWithCategory = (req,res,next)=>{
    knex
    .select('t.id','t.name_ as type','c.category as category')
    .from('type_ as t')
    .innerJoin('category as c',{'t.category_id':'c.id'})
    .where({'c.category':req.params.category})
    .then((rows)=>{
        res.status(201).json({
            count : rows.length,
            types : rows
        })
    }).catch((error)=>{
        console.log(error);
        res.status(500).json({
            message:'an error has occured'
        });
    })
}
//get the specialty with the provided ID , inner join to get the category.
module.exports.getOne = (req,res,next)=>{
    knex.from('type_ as t')
    .where({'t.id':req.params.id})
    .select('t.id','t.name_','c.category')
    .innerJoin('category as c',{'c.id':'t.category_id'})
    .then((rows)=>{
         res.status(200).json({
             'count':rows.length,
             'types':rows
         });
     }).catch((error)=>{
         console.log(error);
         res.status(500).json({
             message:'an error has occured'
         });
     })
 };

module.exports.createOne = async(req,res,next)=>{
    let newSellerType = {};
    newSellerType.name_ = req.body.name;
    newSellerType.id = uuid();
    await knex.select('id').where('category',req.body.category).from('category')
    .then((categoryID)=>{
        newSellerType.category_id = categoryID[0].id;
        knex('type_').insert(newSellerType)
        .then((row)=>{
            res.status(201).json({
                result : row
            })
        }).catch((error)=>{
            console.log(error);
            res.status(500).json({
                message:'an error has occured'
            });
    })
    }).catch((error)=>{
        console.log(error);
        res.status(404).json({
            message:'this category does not exist'
        });
    })
}
//update the name
module.exports.updateOne = (req,res,next)=>{
    knex('type_').where({'id':req.params.id})
    .update(req.body)
    .then((row)=>{
        res.status(204).json({
            message : 'type updated successfully.'
        });
    }).catch((error)=>{
        console.log(error);
        res.status(500).json({
            message:'an error has occured'
        });
    });
}

module.exports.deleteOne = (req,res,next)=>{
    knex('type_').where({'id':req.params.id}).del()
    .then((row)=>{
        res.status(204).json({
            message : 'type deleted successfully.'
        });
    }).catch((error)=>{
        console.log(error);
        res.status(500).json({
            message:'an error has occured'
        });
    });
}