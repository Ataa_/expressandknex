const knex = require('../../knex');
const uuid = require('uuid').v4;

//get all categories
module.exports.getall = (req,res,next)=>{
    knex.select().table('category')
    .then((rows)=>{
        console.log('1')
        res.status(200).json({
            'count':rows.length,
            'categories':rows
        });
    }).catch((error)=>{
        console.log(error);
        res.status(500).json({
            message:'an error has occured'
        });
    })
};

//get all the categories that have specialties
module.exports.getAllWithspecialties = (req,res,next)=>{
    knex.from('category as C')
    .select('C.id','category').distinct()
    .innerJoin('specialty as S',{'C.id':'S.category_id'})
    .then((rows)=>{
         res.status(200).json({
             'count':rows.length,
             'specialties':rows
         });
     }).catch((error)=>{
         console.log(error);
         res.status(500).json({
             message:'an error has occured'
         });
     })
 };

//get all categories that have services
module.exports.getAllWithservices= (req,res,next)=>{
    knex.from('category as C')
    .select('C.id','C.category').distinct()
    .innerJoin('specialty as S',{'C.id':'S.category_id'})
    .innerJoin('service as SV',{'SV.specialty_id':'S.id'})
    .then((rows)=>{
         res.status(200).json({
             'count':rows.length,
             'specialties':rows
         });
     }).catch((error)=>{
         console.log(error);
         res.status(500).json({
             message:'an error has occured'
         });
     })
 };

module.exports.getOne = (req,res,next)=>{
    knex.select().where('id',req.params.id).from('category')
    .then((row)=>{
        res.status(200).json({
            'category':row
        });
    }).catch((error)=>{
        console.log(error);
        res.status(500).json({
            message:'an error has occured'
        });
    })
}

module.exports.createOne = (req,res,next)=>{
    let newCat = req.body;
    newCat.id = uuid();
    knex('category').insert(newCat)
    .then((row)=>{
        res.status(201).json({
            result : row
        })
    }).catch((error)=>{
        console.log(error);
        res.status(500).json({
            message:'an error has occured'
        });
    })
}

module.exports.updateOne = (req,res,next)=>{
    knex('category').where({'id':req.params.id})
    .update(req.body)
    .then((row)=>{
        res.status(204).json({
            message : 'category updatesd successfully.'
        });
    }).catch((error)=>{
        console.log(error);
        res.status(500).json({
            message:'an error has occured'
        });
    });
}

module.exports.deleteOne = (req,res,next)=>{
    knex('category').where({'id':req.params.id}).del()
    .then((row)=>{
        res.status(204).json({
            message : 'category deleted successfully.'
        });
    }).catch((error)=>{
        console.log(error);
        res.status(500).json({
            message:'an error has occured'
        });
    });
}
