const knex = require('../../knex');
const uuid = require('uuid').v4;

//get all services. inner join to get specialties
module.exports.getall = (req,res,next)=>{
    knex.from('service as S').select('S.id','S.name_','description','specialty.name_ as specialty')
    .innerJoin('specialty',{'specialty.id':'S.specialty_id'})
     .then((rows)=>{
         res.status(200).json({
             'count':rows.length,
             'services':rows
         });
     }).catch((error)=>{
         console.log(error);
         res.status(500).json({
             message:'an error has occured'
         });
     })
 };
//get all services under category X
module.exports.getallWithCategory = (req,res,next)=>{
    console.log(req.params.id);
    knex
    .from('service as SV')
    .select('SV.id','SV.name_ as service','SV.description','S.name_ as specialty','C.category as category')
    .join('specialty as S',{'SV.specialty_id': 'S.id'})
    .join('category as C',{'S.category_id':'C.id'})
    .where('C.category', req.params.category)
     .then((rows)=>{
         res.status(200).json({
             'count':rows.length,
             'services':rows
         });
     }).catch((error)=>{
         console.log(error);
         res.status(500).json({
             message:'an error has occured'
         });
     })
 };

 //get all services under specialty X
 module.exports.getallWithSpecialty = (req,res,next)=>{
    console.log(req.params.id);
    knex
    .from('service as SV')
    .select('SV.id','SV.name_ as service','SV.description','S.name_ as specialty')
    .join('specialty as S',{'SV.specialty_id': 'S.id'})
    .where('S.name_', req.params.specialty)
     .then((rows)=>{
         res.status(200).json({
             'count':rows.length,
             'services':rows
         });
     }).catch((error)=>{
         console.log(error);
         res.status(500).json({
             message:'an error has occured'
         });
     })
 };

 module.exports.getOne = (req,res,next)=>{
    knex.from('service as S')
    .where({'S.id':req.params.id})
    .select('S.id','S.name_','Sp.name_ as specialty')
    .innerJoin('specialty as Sp',{'S.specialty_id':'Sp.id'})
    .then((rows)=>{
         res.status(200).json({
             'count':rows.length,
             'services':rows
         });
     }).catch((error)=>{
         console.log(error);
         res.status(500).json({
             message:'an error has occured'
         });
     })
 }

module.exports.createOne = (req,res,next)=>{
    let newService = {};
    newService.name_ = req.body.name;
    newService.description = req.body.description;
    newService.id = uuid();
    knex.select('id').where('name_',req.body.specialty).from('specialty')
    .then((specialtyID)=>{
        newService.specialty_id = specialtyID[0].id;
        knex('service').insert(newService)
        .then((row)=>{
            res.status(201).json({
                result : row
            })
        }).catch((error)=>{
            console.log(error);
            res.status(500).json({
                message:'an error has occured'
            });
    })
    }).catch((error)=>{
        console.log(error);
        res.status(404).json({
            message:'this specialty does not exist'
        });
    })
}

module.exports.updateOne = async(req,res,next)=>{
    knex('service').where({'id':req.params.id})
    .update(req.body)
    .then((row)=>{
        res.status(204).json({
            message : 'service updated successfully.'
        });
    }).catch((error)=>{
        console.log(error);
        res.status(500).json({
            message:'an error has occured'
        });
    });
}

module.exports.deleteOne = (req,res,next)=>{
    knex('service').where({'id':req.params.id}).del()
    .then((row)=>{
        res.status(204).json({
            message : 'service deleted successfully.'
        });
    }).catch((error)=>{
        console.log(error);
        res.status(500).json({
            message:'an error has occured'
        });
    });
}