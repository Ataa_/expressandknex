const express = require('express');
const Router = express.Router();
const sellerController = require('../controllers/seller');


Router.get('/',sellerController.getAll);

Router.get('/:id',sellerController.getOne);

//get all sellers under category X
Router.get('/category/:category',sellerController.getAllwithCategory);

//get all sellers under type X
Router.get('/type/:type',sellerController.getAllwithType);

Router.post('/',sellerController.createOne);

Router.patch('/:id',sellerController.updateOne);

Router.delete('/:id',sellerController.deleteOne);

module.exports = Router; 