const express = require('express');
const Router = express.Router();
const serviceController = require('../controllers/service');



Router.get('/',serviceController.getall);

//get all services under a specific category
Router.get('/category/:category',serviceController.getallWithCategory);

//get all services under a specific specialty
Router.get('/specialty/:specialty',serviceController.getallWithSpecialty);

//Router.get('/withSpecialty',serviceController.getAllWithspecialties);

Router.get('/:id',serviceController.getOne);

Router.post('/',serviceController.createOne);

Router.patch('/:id',serviceController.updateOne);

Router.delete('/:id',serviceController.deleteOne)

module.exports = Router;