const express = require('express');
const Router = express.Router();
const specialtyController = require('../controllers/specialty');



Router.get('/',specialtyController.getall);

Router.get('/:id',specialtyController.getOne);

Router.get('/category/:id',specialtyController.getAllWithCategory);

Router.post('/',specialtyController.createOne);

Router.patch('/:id',specialtyController.updateOne);

//Router.delete('/:id',specialtyController.deleteOne)
module.exports = Router