const express = require('express');
const Router = express.Router();
const categoryController = require('../controllers/category');



Router.get('/',categoryController.getall);
//get all categories that have specialties
Router.get('/withSpecialty',categoryController.getAllWithspecialties);

//get all caategories that have services
Router.get('/withServices',categoryController.getAllWithservices);
Router.get('/:id',categoryController.getOne);

Router.post('/',categoryController.createOne);

Router.patch('/:id',categoryController.updateOne);

Router.delete('/:id',categoryController.deleteOne)
module.exports = Router