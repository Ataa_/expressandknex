const express = require('express');
const Router = express.Router();
const typeController = require('../controllers/type');

Router.post('/',typeController.createOne);

//get all types
Router.get('/',typeController.getAll);
//get all types under a certain category
Router.get('/category/:category',typeController.getAllWithCategory);
//get all types with a certain id
Router.get('/:id',typeController.getOne);

Router.patch('/:id',typeController.updateOne);

Router.delete('/:id',typeController.deleteOne);


module.exports = Router