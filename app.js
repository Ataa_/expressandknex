const express = require('express');
const app = express();
const morgan = require('morgan');
const bp = require('body-parser');
const categoryRoutes = require('./api/routes/category')
const specialtyRoutes = require('./api/routes/specialty');
const serviceRoutes = require('./api/routes/service');
const typeRoutes = require('./api/routes/type');
const sellerRoutes = require('./api/routes/seller');
//using bp to parse incoming requests
app.use(bp.urlencoded({ extended: false }));
app.use(bp.json());
//using morgan as the logger
app.use(morgan('dev'));

app.use('/categories', categoryRoutes);
app.use('/specialties',specialtyRoutes);
app.use('/services',serviceRoutes);
app.use('/seller-types',typeRoutes);
app.use('/sellers',sellerRoutes);
app.use((req,res,next)=>{
    console.log('in app.js')
    res.status(404).json({
        message : 'the requested router was not found'
    });
});

module.exports = app;